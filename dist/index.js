"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.randomCode = void 0;
var random_code_generator_1 = require("./src/random-code-generator");
Object.defineProperty(exports, "randomCode", { enumerable: true, get: function () { return random_code_generator_1.randomCode; } });
